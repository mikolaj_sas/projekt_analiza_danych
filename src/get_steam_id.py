# importing the requests library
import requests

# Counter of recursion
counter = 0
steam_id_list_to_file = []
steam_api_key = ''

# Recursion function
def GetFriendListRecursion(steam_id, file_path, call_limit):
    global counter
    global steam_id_list_to_file
    global steam_api_key
    # api-endpoint
    URL = "https://api.steampowered.com/ISteamUser/GetFriendList/v1"

    #Get request params as a dict
    PARAMS = {'key': steam_api_key, 'steamid': steam_id}

    #Request with params
    r = requests.get(url = URL, params = PARAMS)
    data = r.json()
    if( bool(data) ):
        friendlist = list(data['friendslist']['friends'])
        for friend in friendlist:
            steam_id_list_to_file.append(friend['steamid'])
        while( bool(friendlist) ):
            tmp_steam_id = friendlist[0]['steamid']
            friendlist.pop(0)
            if( counter < call_limit ):
                #TO DO
                # Dodanie steam id do pliku
                print("steam_id: "+steam_id)

                counter = counter + 1
                GetFriendListRecursion(tmp_steam_id, file_path, call_limit)

    r.close()
    return

if __name__ == '__main__':
    GetFriendListRecursion('76561198058694955', 'xd', 10)
    print('cos',len(steam_id_list_to_file))




