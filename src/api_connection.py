import requests
import csv
import random
from pathlib import Path
from datetime import datetime


class ApiConnection:

    """ Kontruktor """
    def __init__(self):
        self.file = open('steamapikey.txt', 'r')
        self.API_KEY = self.file.readline()
        self.INITIAL_ID = "76561198058694955"
        date_now = datetime.now()

        self.game_list = list()
        self.game_player_count = list()
        self.savedIds = list()
        self.current_players_per_day = list()

        Path('current_steam_players/' + str(date_now.date())).mkdir(parents=True, exist_ok=True)

        self.steam_apps = open('steamapps.csv', 'w', encoding='utf-8', newline='')
        self.steam_current_players = open('current_steam_players/'+ str(date_now.date()) + '/current_players_' + str(date_now.hour) + '.csv', 'w', encoding='utf-8', newline='')
        # self.steam_current_players = open('current_players.csv', 'w', encoding='utf-8', newline='')
        self.steam_ids = open('steam_ids.csv', 'w', encoding='utf-8')


    """ Metoda pobierająca json z listą aplikacji zarejestrowanych w sklepie steam, zapisując je do listy i następnie
        wywołując metodę zapisując dane do pliku .csv """

    def get_all_games(self):
        r = requests.get("https://api.steampowered.com/ISteamApps/GetAppList/v2/")
        data = r.json()

        applist = data['applist']
        steam_apps = applist['apps']
        for apps in steam_apps:
            self.game_list.append((apps['appid'], apps['name']))

    """ Metoda pobierająca obiekt który zawiera liczbę graczy aktualnie grających w daną grę """

    def get_number_of_current_players(self):

        appid = 730
        
        r = requests.get("https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=" + str(appid))
        data = r.json()

        data_response = data['response']
        player_count = data_response['player_count']
        game_name = ""

        for game in self.game_list:
            if game[0] == appid:
               game_name = game[1]

    
        self.game_player_count.append((game_name,player_count))

    """ Metoda pobiera pobiera listy znajomych użytkowników i zapisuje ich ID do pliku csv, dane pobierane
     są do uzyskania ich odpowiedniej ilości"""

    def get_steam_idsv(self):

        requests_counter = 0
        savedIds = []
        queue = []

        queue.append(self.INITIAL_ID)
        counter = 0

        while queue and len(savedIds) < 300000:

            r = requests.get("https://api.steampowered.com/ISteamUser/GetFriendList/v1/?key=" + self.API_KEY + "&steamid=" + queue.pop())
            data = r.json()
            counter += 1
            print(counter)
            print(len(savedIds))

            if len(data) != 0:

                friendslist = data['friendslist']
                friends = friendslist['friends']


                for steam_friends in friends:
                    if steam_friends['steamid'] not in savedIds:
                        savedIds.append(steam_friends['steamid'])
                    queue.append(steam_friends['steamid'])

            random.shuffle(queue)
            requests_counter += 1

        self.savedIds = savedIds


    def get_top100games_online_counter(self):

        r = requests.get("https://api.steampowered.com/ISteamApps/GetAppList/v2/")
        data = r.json()
        applist = data['applist']
        game_list = list()
        steam_apps = applist['apps']
        for apps in steam_apps:
            game_list.append((apps['appid'], apps['name']))


        with open("steam_top_100.txt") as f:
            list_of_games = f.readlines()


        for game_ids in list_of_games:
            player_count_request = requests.get("https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1/?appid=" + game_ids)
            player_count_data = player_count_request.json()
            player_count = 0

            if len(player_count_data) != 0:
                response = player_count_data['response']
                player_count = response['player_count']

            app_name = [app[1] for app in game_list if int(game_ids) == app[0]]

            self.current_players_per_day.append((app_name[0], player_count))



    """ Metoda zapisuje pobrane i przefiltrowane dane do plików .csv """

    def save_data_to_csv(self):
        csvwriter = csv.writer(self.steam_apps)
        csvwriter2 = csv.writer(self.steam_current_players)
        csvwriter3 = csv.writer(self.steam_ids)

        print("1")
        csvwriter.writerows(self.game_list)
        print("2")
        print(self.current_players_per_day)
        csvwriter2.writerows(self.current_players_per_day)
        print("3")

        for ids in self.savedIds:

            id = list()
            id.append(ids)
            csvwriter3.writerow(id)

        self.steam_apps.close()
        self.steam_current_players.close()
        self.steam_ids.close()
